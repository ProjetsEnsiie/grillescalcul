from scipy import stats
from time import time
from numpy import linspace, percentile, var, mean
from src.python.generator import RNG
from matplotlib import pyplot as plt
from pyDOE import lhs
from math import pi
import seaborn as sns

N_TO_GENERATE = 100000
SEED = round(time())
from src.python.tests import *

SEED = 1

GENERATORS = {
    "Knuth&Lewis": RNG(SEED, 1664525, 1013904223, pow(2, 32)).generate(),
    "Marsaglia": RNG(SEED, 69069, 0, pow(2, 32)).generate(),
    "Lavaux&Jenssens": RNG(SEED, 31167285, 1, pow(2, 48)).generate(),
    "Haynes": RNG(SEED, 6364136223846793005, 1, pow(2, 64)).generate()
}

def quartiles_moments(k,v):
    print(f"results for {k}")
    print(f"Percentiles (25,50 and 75) : {percentile(v, [25,50,75])}")
    print(f"Variance : {var(v)}, Kurtosis : {stats.kurtosis(v)}, Skewness : {stats.skew(v)}")
    print(f"KSTest : {stats.kstest(v, 'uniform')}")
    print(f"Chi2 : {stats.chisquare(v)}")
    print("*"*80)

def plot_values(val):
    _, axes = plt.subplots(nrows=2, ncols=3)
    bins = linspace(0, 1, 10)
    ax0, ax1, ax2, ax3, ax4, _ = axes.flatten()
    axs = [ax0, ax1, ax2, ax3, ax4]

    for k,v in val.items():
        axe = axs.pop(0)
        axe.hist(v, bins)
        axe.set_title(k)
    plt.show()

def plots_mean(results):
    colors = ["b", "g", "r", "c", "m"]

    results_mean = {k:list(mean(v[:i]) for i in range(len(v))) for k,v in results.items()}
    ax = plt.axes()
    ax.axhline(y=0.5, linestyle='-')
    for k,v in results_mean.items():
        col = colors.pop(0)
        sns.barplot(x=x, y=y1, palette="rocket", ax=v)

        ax.plot(v, color=col, label=k)
        ax.legend()
    plt.show()


def monte_carlo(results):
    _, axes = plt.subplots(nrows=2, ncols=3)
    ax0, ax1, ax2, ax3, ax4, _ = axes.flatten()
    axs = [ax0, ax1, ax2, ax3, ax4]
    SEED = round(time()) + 100
    generators_y = {
        "Knuth&Lewis": RNG(SEED, 1664525, 1013904223, pow(2, 32)).generate(),
        "Marsaglia": RNG(SEED, 69069, 0, pow(2, 32)).generate(),
        "Lavaux&Jenssens": RNG(SEED, 31167285, 1, pow(2, 48)).generate(),
        "Haynes": RNG(SEED, 6364136223846793005, 1, pow(2, 64)).generate()
    }
    results_y = {k:list(next(v) for _ in range(N_TO_GENERATE)) for k,v in generators_y.items()}
    results_y["LHS"] = lhs(1, samples=N_TO_GENERATE)
    for k, v in results.items():
        axe = axs.pop(0)
        axe.set_aspect('equal', 'box')
        count_in = 0
        ok_x, ok_y, nok_x, nok_y = [], [], [], []
        for i in range(N_TO_GENERATE):
            if pow(v[i], 2) + pow(results_y[k][i], 2) < 1:
                count_in += 1
                ok_x.append(v[i])
                ok_y.append(results_y[k][i])
            else:
                nok_x.append(v[i])
                nok_y.append(results_y[k][i])
        axe.scatter(ok_x, ok_y, color="g", s=1)
        axe.scatter(nok_x, nok_y, color="r", s=1)
        axe.set_title(k)
        pi_approximation = (count_in/N_TO_GENERATE) * 4
        print(f"Estimation de pi pour {k} = {pi_approximation}, error with pi : {pi - pi_approximation}")
    plt.show()


def main():
    results = {k:list(next(v) for _ in range(N_TO_GENERATE)) for k,v in GENERATORS.items()}
    results["LHS"] = lhs(1, samples=N_TO_GENERATE)
    # monte_carlo(results)
    # plots_mean(results)
    # for k,v in results.items():
        # quartiles_moments(k,v)
    plot_values(results)

if __name__ == "__main__":
    main()
