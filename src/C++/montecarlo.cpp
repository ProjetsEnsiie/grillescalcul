#include <time.h>
#include <mutex>
#include <iostream>
#include <thread>
#include <atomic>
#include <math.h>
#include <ctime>



std::mutex mutex_1;
std::atomic<int> pt_in_atomic ; // creates an atomic int
const float PT_TOTAL = 100000000;
int pt_in = 0;
float resultArray[4];
const int nbPhysicalCore = 4;

void montecarlo(int threadnum){
    int i=(threadnum)*(PT_TOTAL/4);
    float x,y;
    unsigned int seed = threadnum;

    while (i<=(PT_TOTAL/4)*(threadnum+1))
    {
        x = (double)rand_r(&seed) / RAND_MAX;
        y = (double)rand_r(&seed) / RAND_MAX;
        if (x*x + y*y <= 1)
        {
            mutex_1.lock();
            pt_in++;
            mutex_1.unlock();
        }
        i++;
    }
}
void montecarlo_atomic(int threadnum){
    int i=(threadnum)*(PT_TOTAL/4);
    float x,y;
    unsigned int seed = threadnum*10;

    while (i<=(PT_TOTAL/4)*(threadnum+1))
    {
      x = (double)rand_r(&seed) / RAND_MAX;
        y = (double)rand_r(&seed) / RAND_MAX;
        if (x*x + y*y <= 1)
        {
            pt_in_atomic++;
        }
        i++;
    }
}

void montecarlo_version_1_1_0(int threadnum){
    int thread_pt_in = 0;
    float x,y;
    unsigned int seed = threadnum*100;

    pt_in_atomic=0;
    int i=(threadnum)*(PT_TOTAL/4);
    while (i<=(PT_TOTAL/4)*(threadnum+1))
    {
        x = (double)rand_r(&seed) / RAND_MAX;
        y = (double)rand_r(&seed) / RAND_MAX;
        if (x*x + y*y <= 1)
        {
            thread_pt_in++;
        }
        i++;
    }
    resultArray[threadnum]=thread_pt_in;
}

int main()
{   
    float globalresult=0;
    std::thread thread_1_1s[4];
    std::thread threads[4];
    std::srand(std::time(nullptr)); // use current time as seed for random generator

    std::clock_t start;
    double duration;

    start = std::clock();

    // montecarlo avec lock
    for(unsigned int i=0; i<nbPhysicalCore; i++) threads[i] = std::thread(montecarlo, i);
    for(auto& thread : threads) thread.join();
    std::cout << "MonteCarlo avec lock : "<< (pt_in*4)/PT_TOTAL << " (" << M_PI-((pt_in*4)/PT_TOTAL) << ") "<< std::endl;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"durée: "<< duration/nbPhysicalCore << std::endl;
    start = std::clock();


    // montecarlo avec atomic
    for(unsigned int i=0; i<nbPhysicalCore; i++) threads[i] = std::thread(montecarlo_atomic, i);
    for(auto& thread : threads) thread.join();
    std::cout << "MonteCarlo avec atomic : " << (pt_in_atomic*4)/PT_TOTAL << " (" << M_PI-((pt_in_atomic*4)/PT_TOTAL) << ") "<<std::endl;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"durée: "<< duration/nbPhysicalCore << std::endl;
    start = std::clock();

    // montecarlo avec fusion des résultats
    for(unsigned int i=0; i<nbPhysicalCore; i++) thread_1_1s[i] = std::thread(montecarlo_version_1_1_0, i);
    for(auto& thread : thread_1_1s) thread.join();
    for(int i=0;i<nbPhysicalCore;i++){
        globalresult+=resultArray[i];
    }
    std::cout << "Montecarlo somme par thread : " << (globalresult*4)/PT_TOTAL <<" (" << M_PI-((globalresult*4)/PT_TOTAL) << ") "<< std::endl;
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"durée: "<< duration/nbPhysicalCore << std::endl;
    start = std::clock();
    return 0;
}