#include <time.h>
#include <mutex>
#include <iostream>
#include <thread>
#include <ctime>
#include <omp.h>
#include <math.h>
#include <random>

const int PT_TOTAL = 100000000;
std::mutex mutex_1;
const int nbPhysicalCore = 4;


int montecarlmarx(int taille,int threads){

    int pt_in = 0;
    float x,y;
    unsigned int seed;
    
    #pragma omp parallel private(seed,x, y) reduction( + : pt_in ) num_threads(threads)
    {
        seed =  omp_get_thread_num();
        #pragma omp  for 
        for (unsigned int i=1; i<=taille; i++)
        {
            x = (double)rand_r(&seed) / RAND_MAX;
		    y = (double)rand_r(&seed) / RAND_MAX;
			if (x*x + y*y <= 1) pt_in++;
        }
    }
    return pt_in;
}
int main()
{   

    std::clock_t start;
    double duration;
    double time,start_time;
    std::cout << "temps par thread pour " << PT_TOTAL << std::endl;
    for (int thread = 1;thread <= 4;thread++){
        start_time = omp_get_wtime();


        int pt_in = montecarlmarx(PT_TOTAL,thread);
        time = omp_get_wtime() - start_time;
        std::cout<< thread <<" thread: "<< time << std::endl;

    }
    std::cout << "temps pour "<< PT_TOTAL*10 <<" points pour 4 threads" << std::endl;
    for (int taille=1000;taille<=PT_TOTAL*10;taille=taille*10){
        start_time = omp_get_wtime();

        int pt_in = montecarlmarx(taille,4);
        time = omp_get_wtime() - start_time;

        std::cout<< taille << " points : "<< time << std::endl;

    }

    start = std::clock();

    return 0;
}