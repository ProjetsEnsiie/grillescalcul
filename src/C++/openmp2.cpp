#include <time.h>
#include <iostream>
#include <fstream>
#include <math.h>

#include <omp.h>
#include <random>
using namespace std;

const int PT_TOTAL = 100000000;
const int nbPhysicalCore = 4;


int montecarlmarx(int taille,int threads,ofstream *myfile ){

    int pt_in = 0;
    float x,y;
    unsigned int seed;
    
    #pragma omp parallel private(seed,x, y) reduction( + : pt_in ) num_threads(threads)
    {
        seed =  omp_get_thread_num();
        #pragma omp  for 
        for (unsigned int i=1; i<=taille; i++)
        {
            x = (double)rand_r(&seed) / RAND_MAX;
		    y = (double)rand_r(&seed) / RAND_MAX;
			if (x*x + y*y <= 1) pt_in++;
        }
        #pragma omp critical
        {
            float result = (float) (pt_in*4)/taille;
            *myfile << "thread n"<<seed<<" : "<< result  << " (" << M_PI- result << ") " << std::endl;
        }

    }
    return pt_in;
}
int main()
{   

        ofstream myfile;
        myfile.open ("example.txt",std::ios_base::app);
        int pt_in = montecarlmarx(PT_TOTAL,4,&myfile);
        float result = (float) (pt_in*4)/PT_TOTAL;

        myfile << "total : "<< result  << " (" << M_PI- result << ") " << std::endl;

        // std::cout << "Taille : "<< taille  << std::endl;
        // float result = (float) (pt_in*4)/taille;
        std::cout << "Resultat ecrit dans exemple.txt" << std::endl;

    

    return 0;
}