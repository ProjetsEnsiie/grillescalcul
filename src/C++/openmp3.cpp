#include <time.h>
#include <iostream>
#include <fstream>
#include <math.h>

#include <omp.h>
#include <random>
using namespace std;

const int PT_TOTAL = 10000;
const int nbPhysicalCore = 4;
struct point
{
    double x;
    double y;
};
float distance(int x1, int x2, int y1, int y2) 
{ 
    return sqrt(pow(x2 - x1, 2) +  
                pow(y2 - y1, 2) * 1.0); 
}
void genererMillePoints(point pointArray[PT_TOTAL]){
    point a;
    unsigned int seed;
    
    #pragma omp parallel private(seed,a)  num_threads(4)
    {
        seed =  omp_get_thread_num();
        #pragma omp  for 
        for (unsigned int i=1; i<PT_TOTAL; i++)
        {

            a.x = (double)rand_r(&seed) / (RAND_MAX/10);
           
		    a.y = (double)rand_r(&seed) / (RAND_MAX/10);
            pointArray[i]= a;
		   
           // std::cout << "X("<< x1<<"," <<x2 <<") Y(" << y1<<";" <<y2 <<")" << std::endl;
        }       
    }
}
double calculerDistancesParallel(point pointArray[PT_TOTAL]){
    double sum = 0;
    #pragma omp parallel for collapse(2) reduction( + : sum )
    for (unsigned int i=0; i<10; i++)
    {
    // this loop will be collapsed with the outer loop
        for (unsigned int j=0; j<10; j++)
        {
           sum += distance(pointArray[i].x,pointArray[i].y,pointArray[j].x,pointArray[j].y);
        }
    }
    return sum;
}
double calculerDistances(point pointArray[PT_TOTAL]){
    double sum = 0;
    for (unsigned int i=0; i<PT_TOTAL; i++)
    {
    // this loop will be collapsed with the outer loop
        for (unsigned int j=0; j<PT_TOTAL; j++)
        {
           sum += distance(pointArray[i].x,pointArray[i].y,pointArray[j].x,pointArray[j].y);
        }
    }
    return sum;
}
int main()
{   
    point pointArray[PT_TOTAL];
    double sommeDistances=0;
    genererMillePoints(pointArray);
    
    // for(int i=0;i<PT_TOTAL;i++){
    //     std::cout <<pointArray[i].x << " " <<pointArray[i].y <<std::endl;
    // } 

    // Somme des distances sans thread
    double start_time = omp_get_wtime();
    sommeDistances = calculerDistances(pointArray);
    std::cout << "sum sans thread : "<< sommeDistances  << std::endl;
    double time = omp_get_wtime() - start_time;
    std::cout<<"temps: " <<time << "s" << std::endl;
    
    // Somme des distances avec thread
    start_time = omp_get_wtime();
    std::cout << "sum avec thread : "<< sommeDistances  << std::endl;
    sommeDistances = calculerDistancesParallel(pointArray);
    time = omp_get_wtime() - start_time;
    std::cout<< "temps: "<<time << "s" << std::endl;

    

    return 0;
}