#include <iostream>
#include <thread>
#include <array>
#include <mutex>
#include <string>
#include <chrono>
const int LOOP = 500;
int count = 0;
std::mutex mutex_1;
std::mutex mutex_2;

void datarace_a() {
    count++;
}

void deadlock_of_doom_1(){
    mutex_1.lock();
    mutex_2.lock();
    mutex_1.unlock();
    mutex_2.unlock(); // release access

}

void deadlock_of_doom_2(){
    mutex_2.lock();
    std::this_thread::sleep_for (std::chrono::seconds(1));
    mutex_1.lock(); // release access

    mutex_2.unlock(); // release access
    mutex_1.unlock(); // release access

}

void reponse_coucou(std::string moi, std::string dest){
    for (int i = 0; i < LOOP; i++)
    {
        mutex_1.lock(); // block access
        mutex_2.lock();
        mutex_1.unlock(); // release access
        mutex_2.unlock();
    }
}


void coucou(std::string moi, std::string dest){
    for (int i = 0; i < LOOP; i++)
    {
            /* code */
        mutex_2.lock();
        mutex_1.lock(); // block access
        mutex_2.unlock();
        mutex_1.unlock(); // release access
    }
    

}
int main(int argc, char const *argv[])
{
    std::thread threads[10000];
    // *****Race********
    // for(unsigned int i=0; i<10000; i++) threads[i] = std::thread(datarace_a);
    // // joins all threads
    // for(auto& thread : threads) thread.join();
    // std::cout << count << std::endl;
        
    // std::thread theo(coucou,"theo","remy");
    // std::thread remy(reponse_coucou,"remy","theo");
    // theo.join();
    // remy.join();
    std::thread dl1(deadlock_of_doom_1);
    std::thread dl2(deadlock_of_doom_2);
    dl1.join();
    dl2.join();
    return 0;
}
