#include <iostream>
#include <chrono>
#include <thread>
#include <array>
#include <ctime>

int tab[1000];
int counter = 0;
int working[3];
const unsigned int nbPhysicalCore = std::thread::hardware_concurrency();


int f(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(3  * 100));
    return num + 3;
}

int g(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(4 * 100));
    return num + 4;
}

int h(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(5 * 100));
    return num + 5;
}

void fillTabretard(int index){
    tab[index] = f(index);
}

void dispatchTread(int threadnumber){
    for(int index=250*threadnumber;index<250*(threadnumber+1);index++){
        fillTabretard(index);
    }
}

void slave(int *work)
{
    bool in_progress = true;
    while(in_progress){
        if(*work>=0){

            fillTabretard(*work);
            *work=-1;
        } 
        else if (*work == -2)  {
            in_progress = false;
        }
    }
    std::cout << "worker ending"<< std::endl ;

}

void master(int num)
{
    std::thread slaves[3];
    int work[3]={-1,-1,-1};
    slaves[0]=std::thread (slave, &work[0]);
    slaves[1]=std::thread (slave, &work[1]);
    slaves[2]=std::thread (slave, &work[2]);

    for(int i=0;i<num;i++){
        int j=0;
        bool loop=true;
        while(loop){
            if (work[j%3]<0){
                work[j%3]=i;
                loop=false;
            }
            j++;
        }
    }
    std::this_thread::sleep_for (std::chrono::milliseconds(500));

    work[0]=-2;
    work[1]=-2;
    work[2]=-2;

    for(auto& thread : slaves) thread.join();

}

int main(int argc, char const *argv[])
{
    std::thread threads[4];
    std::clock_t start;
    double duration;
    start = std::clock();
        printf ("Calculating...\n");

    // for(unsigned int i=0; i<nbPhysicalCore; i++) threads[i] = std::thread(dispatchTread, i);
    // for(auto& thread : threads) thread.join();
    clock_t t;
    std::thread master_thread(master, 100);
    master_thread.join();

    std::cout<<"printf: "<< ( std::clock() - start ) / (double) CLOCKS_PER_SEC <<'\n';
    // start = std::clock();
    // printf ("Calculating...\n");

    // for(unsigned int i=0; i<nbPhysicalCore; i++) threads[i] = std::thread(dispatchTread, i);
    // for(auto& thread : threads) thread.join();

    // std::cout<<"printf: "<< ( std::clock() - start ) / (double) CLOCKS_PER_SEC <<'\n';
    for(int j=0;j<100;j++){
        std::cout << "tab value" << tab[j] << '\n';
    }
    return 0;
}
