#include <iostream>
#include <chrono>
#include <thread>
#include <array>
int tab[1000];


int f(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(3 * num * 100));
    return num + 3;
}

int g(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(4 * num * 100));
    return num + 4;
}

int h(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(5 * num * 100));
    return num + 5;
}

void fillTab(int index){
    tab[index]=index;
}
void dispatchTread(int treadNumber){
    for(int i=0;i<1000;i++){
        if(i%treadNumber==0){
            fillTab(i);
            std::cout << "thread, case" << treadNumber << '\n';

        }
    }
}
int main(int argc, char const *argv[])
{
    const unsigned int nbPhysicalCore = std::thread::hardware_concurrency();
    std::thread threads[nbPhysicalCore];
    for(unsigned int i=0; i<nbPhysicalCore; i++) threads[i] = std::thread(fillTab, i);
    for(int j=0;j<1000;j++){
         std::cout << "tab value" << tab[j] << '\n';

    }
    for(auto& thread : threads) thread.join();
    return 0;
}
