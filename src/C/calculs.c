#include <thread>
#include<iostream>


void f(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(3 * num * 100));
}

void g(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(4 * num * 100));
}

void h(int num)
{
    std::this_thread::sleep_for (std::chrono::milliseconds(5 * num * 100));
}


   
// displays the id of a thread and exits
void displayId(unsigned int treadNumber)
{
    const std::thread::id threadId = std::this_thread::get_id();
    std::cout << "- thread number: " << treadNumber << " Id: " << threadId << '\n';
}
// creates thread, displays their ID and exits
void main()
{
    // displays the number of cores on the machine
    const unsigned int nbPhysicalCore = std::thread::hardware_concurrency();
    std::cout << "There are " << nbPhysicalCore << " physical cores on this machine." << '\n';
    // creates one thread per core and detach them
    for(unsigned int i = 0; i < nbPhysicalCore; i++)
    {
        std::thread ti(displayId, i);
        ti.detach();
    }
}