SEED = 0
MULTIPLIER = 31415821
INCREMENT = 1
MODULUS = pow(10,8)

class RNG:
    def __init__(self, seed, multiplier, increment, modulus):
        self.multiplier = multiplier
        self.increment = increment
        self.modulus = modulus
        self.seed = seed

    def generate(self):
        while True:
            new_seed = ((self.multiplier * self.seed + self.increment)) % self.modulus
            self.seed = new_seed
            yield new_seed/(self.modulus)

if __name__ == "__main__":
    gen = RNG(SEED, MULTIPLIER, INCREMENT, MODULUS).generate()
    while True:
        print(next(gen))
        input()